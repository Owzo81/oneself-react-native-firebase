import React, { useState, useContext } from 'react';
import { StyleSheet, Text, Dimensions, TouchableOpacity, Image, View, KeyboardAvoidingView } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { FirebaseContext } from '@components/Firebase';
import TextInputField from '@components/Form/TextInputField';

import { onHandleForgotPassword, onHandleSignUp, onHandleLogin } from './lib'

const Login = ({ navigation }) => {
  const firebase = useContext(FirebaseContext);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);

  const isInvalid = email === '' || password === '';

  return (
    <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={styles.container}>
      <View style={styles.logocontainer}>
        <Image
          style={styles.logo}
          source={require('../../../assets/images/logo.png')}
        />
      </View>
      <View style={styles.formContainer}>
        <TextInputField
          value={email}
          onChangeValue={(text) => setEmail(text)}
          password={false}
          placeholder="Email..."
          icon="email"
        />
        <TextInputField
          value={password}
          onChangeValue={(text) => setPassword(text)}
          password={true}
          placeholder="Password..."
          icon="password"
        />

        {error && <Text style={styles.errorText}>{error.message}</Text>}

        <TouchableOpacity style={styles.button} disabled={isInvalid} onPress={() => onHandleLogin(email, password, setError, firebase, navigation)}>
          <Text style={styles.buttontext}>Login</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.link} onPress={() => onHandleForgotPassword(navigation)}>
          <Text style={styles.linkText}>Forgotten your password?</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.link} onPress={() => onHandleSignUp(navigation)}>
          <Text style={styles.signup}>Not got an account? Sign up</Text>
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>
  );
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#c3ebbb'
  },
  logocontainer: {
    marginTop: 90,
    marginBottom: 20,
    flex: 2,
    alignItems: 'center' ,
    justifyContent: 'center',
  },
  logo: {
    width: Dimensions.get('screen').width - 100,
    height: Dimensions.get('screen').height - 600,
  },
  formContainer: {
    flex: 3,
    alignItems: 'center',
  },
  buttoncontainer: {
    flex: 1,
    alignItems: 'center',
  },
  button: {
    marginTop: 30,
    backgroundColor: '#4c4d51',
    paddingVertical: 10,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
  },
  buttontext: {
    textAlign: 'center',
    color: '#32a852',
    fontFamily: 'sans-serif-thin',
    fontSize: 17,
    fontWeight: 'bold'
  },
  link: {
    marginTop: 10,
    paddingVertical: 10,
    width: Dimensions.get('screen').width - 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
  },
  linkText: {
    paddingTop: 10,
    color: '#4c4d51',
    fontWeight: 'bold'
  },
  signup: {
    color: '#32a852',
    fontWeight: 'bold'
  },
  errorText: {
    color: 'red'
  }
});
