import react from 'react';

export const onHandleForgotPassword = (navigation) => navigation.navigate('ForgotPassword');
export const onHandleSignUp = (navigation) => navigation.navigate('SignUp');

export const onHandleLogin = async (email, password, setError, firebase, navigation) => {
  try {
    const response = await firebase.loginWithEmail(email.trim(), password.trim())

    if (response.user) {
      navigation.navigate('MainTabNavigator')
    }
  } catch (error) {
    setError(error);
  }
}
