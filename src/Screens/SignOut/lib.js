import react from 'react';

export const handleSignout = async (firebase, navigation) => {
  try {
    await firebase.signOut()
    navigation.navigate('Login')
  } catch (error) {
    alert(error)
  }
}
