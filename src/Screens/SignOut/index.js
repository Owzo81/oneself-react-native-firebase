import React, { useContext, useEffect } from 'react';
import { FirebaseContext } from '../../Components/Firebase';

import { handleSignout } from './lib';

export default function SignOut({ navigation }) {
  const firebase = useContext(FirebaseContext);

  useEffect(() => {
    handleSignout(firebase, navigation)
  });

  return (null);
}
