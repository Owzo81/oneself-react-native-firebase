import react from 'react';

export const onDateChange = (date, navigation) => {
  date = new Date(date);
  navigation.navigate('DayScreen',
    {
      date: date
    }
  );
}
