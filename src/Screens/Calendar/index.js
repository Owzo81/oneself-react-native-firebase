import React, { useContext, useState } from 'react';
import { StyleSheet, Text, Dimensions, Button, View, TouchableOpacity } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { FirebaseContext } from '../../Components/Firebase';

import CalendarPicker from 'react-native-calendar-picker';

import { onDateChange } from './lib';

export default function Calendar({ navigation }) {
  const firebase = useContext(FirebaseContext);

  const [bannerDate, setBannerDate] = useState(new Date());
  let bannerDateFormatted = bannerDate.toDateString();

  return (
    <View style={styles.container}>
      <View style={styles.calendarcontainer}>
      <Text style={styles.field}>{bannerDateFormatted}</Text>
        <View style={styles.calendar}>
          <CalendarPicker
            onDateChange={(date) => {setBannerDate(new Date(date))}}
            textStyle={{
              fontFamily: 'sans-serif',
              color: 'black',
              fontWeight: 'bold'
            }}
            todayTextStyle={{
              fontFamily: 'sans-serif',
              color: 'grey',
              fontWeight: 'bold'
            }}
            selectedDayColor="black"
            selectedDayTextColor="white"
            todayBackgroundColor="#c3ebbb"
            previousTitle=" <<<"
            nextTitle=">>> "
          />
        </View>
      </View>

      <MaterialIcons style={styles.button} name="navigate-before" size={150} color="#4c4d51"
        onPress={() => onDateChange(bannerDate, navigation)} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c3ebbb',
  },
  calendarcontainer: {
    borderRadius: 15,
    borderWidth: 1,
    width: "95%",
    height: "65%",
    backgroundColor: 'gray',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
  },
  calendar: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 15,
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
  },
  field: {
    width: "100%",
    height: "15%",
    flexDirection: 'row',
    borderRadius: 15,
    borderWidth: 1,
    marginTop: 10,
    alignItems: 'center',
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    fontFamily: 'sans-serif-thin',
    fontSize: 30,
    color: 'black',
  },
  button: {
    width: "95%",
    height: "9%",
    flexDirection: 'row',
    borderRadius: 15,
    borderWidth: 1,
    marginTop: 10,
    alignItems: 'center',
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: 'gray',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    fontFamily: 'sans-serif-thin',
    fontSize: 40,
    color: 'white',
  }
});
