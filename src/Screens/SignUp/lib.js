import react from 'react';

export const onHandleSignUp = (email, password, setError, firebase) => {
  firebase.signUpWithEmail(email, password)
  .then(authUser => {
    // Create a user in your Firebase realtime database
     firebase.createNewUser(authUser.user.uid)
      .set({
        name,
        email,
        role: 'CUSTOMER',
      })
      .then((data) => {
        console.log('data ' , data)
      })
      .catch((error)=>{
        setError(error);
      })
  })
  .catch(error => {
    setError(error);
  });
}
