import React, { useState, useContext } from 'react';
import { StyleSheet, Text, Dimensions, TouchableOpacity, View } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Octicons } from '@expo/vector-icons';

import { FirebaseContext } from '@components/Firebase';
import TextInputField from '@components/Form/TextInputField';

import { onHandleSignUp } from './lib';

const SignUp = ({ navigation }) => {
  const firebase = useContext(FirebaseContext);

  const [name, setName] = useState('');
  const [test, setTest] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);

  const isInvalid = name === '' || email === '' || password === '';

  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <Text style={styles.titletext}>Please enter your details to sign up...</Text>
        <TextInputField
          value={name}
          onChangeValue={(text) => setName(text)}
          password={false}
          placeholder="Enter your full name..."
          icon="person"
        />
        <TextInputField
          value={email}
          onChangeValue={(text) => setEmail(text)}
          password={false}
          placeholder="Email..."
          icon="email"
        />
        <TextInputField
          value={password}
          onChangeValue={(text) => setPassword(text)}
          password={true}
          placeholder="Password..."
          icon="password"
        />

        {error && <Text style={styles.errorText}>{error.message}</Text>}

        <TouchableOpacity style={styles.button} disabled={isInvalid} onPress={() => onHandleSignUp(email, password, setError, firebase)}>
          <Text style={styles.buttontext}>Sign Up</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default SignUp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#c3ebbb'
  },
  formContainer: {
    flex: 1,
    marginTop: Dimensions.get('screen').height - 750,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  titletext: {
    paddingBottom: 10,
  },
  button: {
    marginTop: 30,
    backgroundColor: '#4c4d51',
    paddingVertical: 10,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
  },
  buttontext: {
    textAlign: 'center',
    color: '#32a852',
    fontFamily: 'sans-serif-thin',
    fontSize: 17,
    fontWeight: 'bold'
  },
  errorText: {
    color: 'red'
  }
});
