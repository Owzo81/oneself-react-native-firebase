import React from 'react';
import renderer from 'react-test-renderer';
import Goals from '@screens/Goals';
import { FirebaseContext } from '@components/Firebase';

const goalsForUser = {
  "Box a chicken": {
    "name": "Box a chicken",
    "percentComplete": "40",
  },
  "Marry a model": {
    "name": "Marry a model",
    "percentComplete": "10",
  },
  "Poop on the roof": {
    "name": "Poop on the roof",
    "percentComplete": "70",
  },
  "Punch Adz": {
    "name": "Punch Adz",
    "percentComplete": "30",
  },
  "Scratch Bum": {
    "name": "Scratch Bum",
    "percentComplete": 0,
  },
  "Slap some soup": {
    "name": "Slap some soup",
    "percentComplete": "10",
  },
  "Walk the dog": {
    "name": "Walk the dog",
    "percentComplete": "55",
  },
  "Wash the car": {
    "name": "Wash the car",
    "percentComplete": "0",
  },
  "Watch netflix": {
    "name": "Watch netflix",
    "percentComplete": "100",
  }
};

const getGoalsForUser = jest.fn();
getGoalsForUser.mockReturnValue(goalsForUser);

const firebase = {
  getGoalsForUser: getGoalsForUser
};

test('renders correctly', () => {
  const tree = renderer.create(
    <FirebaseContext.Provider value={firebase}>
      <Goals />
    </FirebaseContext.Provider>
    ).toJSON();
  expect(tree).toMatchSnapshot();
});
