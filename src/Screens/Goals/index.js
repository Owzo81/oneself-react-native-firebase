import React, { useContext, useState, useEffect } from 'react';
import { StyleSheet, Text, FlatList, Modal, TouchableOpacity, Dimensions, View } from 'react-native';
import { FirebaseContext } from '@components/Firebase';
import GoalTile from '@components/GoalTile';
import TextInputField from '@components/Form/TextInputField';

import { onHandleCreateGoal, getGoalsForUser } from './lib';

const Goals = () => {
  const firebase = useContext(FirebaseContext);

  const [currentUser, setCurrentUser] = useState(firebase.getAuthUser());
  const [error, setError] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [name, setName] = useState('');
  const [goals, setGoals] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const isInvalid = name === '';

  useEffect(() => {
    getGoalsForUser(currentUser, firebase, setGoals, setIsLoading);
  }, [currentUser]);

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {}}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>Create your new goal!</Text>
            <TextInputField
              value={name}
              onChangeValue={(text) => setName(text)}
              password={false}
              placeholder="Enter a name for your goal..."
              icon="person"
            />
            <TouchableOpacity style={styles.button} disabled={isInvalid} onPress={() => {
              onHandleCreateGoal(name, currentUser.uid, setError, firebase);
              setName('');
              setModalVisible(!modalVisible);
            }}>
              <Text style={styles.buttontext}>Create goal</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={() => setModalVisible(!modalVisible)}>
              <Text style={styles.buttontext}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      {goals &&
        <FlatList
          style={styles.listcontainer}
          data={Object.keys(goals)}
          renderItem={({ item }) => (
            <GoalTile item={goals[item]} />
          )}
        />
      }

      <TouchableOpacity style={styles.button} onPress={() => setModalVisible(true)}>
        <Text style={styles.buttontext}>Add new goal</Text>
      </TouchableOpacity>

      {error && <Text style={styles.errorText}>{error.message}</Text>}
    </View>
  );
};

export default Goals

const styles = StyleSheet.create({
  centeredView: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#c3ebbb',
    paddingBottom: "10%"
  },
  listcontainer: {
    height: "70%",
    width: "95%",
    marginTop: "5%"
  },
  modalView: {
    margin: 20,
    width: Dimensions.get('screen').width - 40,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  },
  button: {
    marginTop: 30,
    backgroundColor: '#4c4d51',
    paddingVertical: 10,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row'
  },
  buttontext: {
    textAlign: 'center',
    color: '#32a852',
    fontFamily: 'sans-serif-thin',
    fontSize: 17,
    fontWeight: 'bold'
  }
});
