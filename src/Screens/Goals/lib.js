import react from 'react';

export const getGoalsForUser = (currentUser, firebase, setGoals, setIsLoading) => {
  firebase.getGoalsForUser(currentUser.uid).on('value', snapshot => {
    const goals = snapshot.val();
    if (goals) {
      setGoals(goals);
      setIsLoading(false);
    } else {
      setGoals(null);
      setIsLoading(false);
    }
  });
}

export const onHandleCreateGoal = (name, currentUserId, setError, firebase) => {
  firebase.createNewGoal(currentUserId, name)
    .set({
      name: name,
      percentComplete: 0
  }).then(() => {
      alert("Entry added for goal " + name);
  }).catch((error)=>{
    setError(error);
  });
}
