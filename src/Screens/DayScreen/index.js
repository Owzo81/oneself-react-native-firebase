import React, { useContext, useState, useEffect } from 'react';
import { StyleSheet, Text, Button, View } from 'react-native';
import { FirebaseContext } from '../../Components/Firebase';
import Day from '@components/Day'

export default function DayScreen({ navigation }) {
  const firebase = useContext(FirebaseContext);

  const [currentUser, setCurrentUser] = useState(firebase.getAuthUser());

  return (
    <Day navigation={navigation} firebase={firebase}
         date={navigation.getParam('date')} currentUserId={currentUser.uid}/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
});
