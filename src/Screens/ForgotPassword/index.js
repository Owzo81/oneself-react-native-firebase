import React, { useState, useContext } from 'react';
import { StyleSheet, Text, Dimensions, TouchableOpacity, View } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { FirebaseContext } from '@components/Firebase';
import TextInputField from '@components/Form/TextInputField';

import { onHandleForgotPassword } from './lib';

const ForgotPassword = ({ navigation }) => {
  const firebase = useContext(FirebaseContext);

  const [email, setEmail] = useState('');
  const [error, setError] = useState(null);
  const [message, setMessage] = useState(null);

  const isInvalid = email === '';

  return (
    <View style={styles.container}>
      <View style={styles.formcontainer}>
        <Text style={styles.titletext}>Please enter the email for the account...</Text>
        <TextInputField
          value={email}
          onChangeValue={(text) => setEmail(text)}
          password={false}
          placeholder="Email..."
          icon="email"
        />

        {error && <Text style={styles.errortext}>{error.message}</Text>}
        {message && <Text style={styles.messagetext}>{message}</Text>}

        <TouchableOpacity style={styles.button} disabled={isInvalid} onPress={() => onHandleForgotPassword(email, setMessage, setError, firebase)}>
          <Text style={styles.buttontext}>Submit</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default ForgotPassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent:'flex-start',
    alignItems: 'center',
    backgroundColor: '#c3ebbb'
  },
  formcontainer: {
    flex: 1,
    marginTop: Dimensions.get('screen').height - 750,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  button: {
    marginTop: 30,
    backgroundColor: '#4c4d51',
    paddingVertical: 10,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
  },
  buttontext: {
    textAlign: 'center',
    color: '#32a852',
    fontFamily: 'sans-serif-thin',
    fontSize: 17,
    fontWeight: 'bold'
  },
  titletext: {
    paddingBottom: 10,
  },
  errortext: {
    color: 'red'
  },
  messagetext: {
    color: 'green'
  }
});
