import react from 'react';

export const onHandleForgotPassword = async (email, setMessage, setError, firebase) => {
  await firebase.doPasswordReset(email)
  .then(() => {
    setMessage('Email sent successfully');
  })
  .catch(error => {
    setError(error);
  });
}
