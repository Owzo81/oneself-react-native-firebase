import react from 'react';

export const onHandleInsertEntry = (entry, longitude, latitude, date, currentUserId, firebase, navigation) => {
  firebase.createNewJournalEntry(date, currentUserId)
    .set({
      journalentry: entry,
      longitude: longitude,
      latitude: latitude,
  }).then( () => {
      alert("Entry added for date " + date);
      navigation.navigate("HomeScreen");
  });
}

export const getCurrentLocation = (setLongitude, setLatitude) => {
  navigator.geolocation.getCurrentPosition(
      (position) => {
          setLongitude(position.coords.longitude);
          setLatitude(position.coords.latitude);
      },
      (error) => {
          console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
  );
}
