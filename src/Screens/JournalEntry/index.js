import React, { useState, useContext, useEffect } from 'react';
import { StyleSheet, Text, TextInput, Dimensions, TouchableOpacity, Image, View, KeyboardAvoidingView } from 'react-native';
import { FirebaseContext } from '@components/Firebase';
import { MaterialIcons } from '@expo/vector-icons';

import { onHandleInsertEntry, getCurrentLocation } from './lib'

const JournalEntry = ({ navigation }) => {
  const firebase = useContext(FirebaseContext);

  const [entry, setEntry] = useState('');
  const [error, setError] = useState(null);
  const [longitude, setLongitude] = useState(null);
  const [latitude, setLatitude] = useState(null);

  const { date, currentUserId } = navigation.state.params;
  const isInvalid = entry === '';

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView behavior={Platform.OS == "ios" ? "padding" : "height"} style={styles.formContainer}>
      <View style={styles.iconsheader}>
        <MaterialIcons style={styles.iconlocation} name="add-location" size={40} color="white"
          onPress={() => {
              getCurrentLocation(setLongitude, setLatitude);
              alert("Location set!");
            }
          }
        />
      </View>
          <TextInput
            multiline
            numberOfLines={16}
            style={styles.textInput}
            onChangeText={text => setEntry(text)}
            value={entry}
            placeholder="Enter amazing journal entry..."
            placeholderTextColor="#4c4d51"
          />

          {error && <Text style={styles.errorText}>{error.message}</Text>}

        <TouchableOpacity style={styles.button} disabled={isInvalid} onPress={() => onHandleInsertEntry(entry, longitude, latitude, date, currentUserId, firebase, navigation)}>
          <Text style={styles.buttontext}>Save entry</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  );
}

export default JournalEntry;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#c3ebbb'
  },
  backgroundImage: {
    flex: 1,
    width: Dimensions.get('screen').width
  },
  formContainer: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  formfield: {
    marginTop: 15,
    width: Dimensions.get('screen').width - 30,
    height: Dimensions.get('screen').height - 600,
    flexDirection: 'row',
    borderRadius: 15,
    alignItems: 'center',
    backgroundColor: '#32a85230',
    marginVertical: 4,
    paddingLeft: 20,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOpacity: 1,
    shadowRadius: 10,
    shadowOffset: { width: 0, height: 1 },
    elevation: 10
  },
  buttoncontainer: {
    flex: 1,
    alignItems: 'center'
  },
  titletext: {
    paddingBottom: 10,
  },
  button: {
    marginTop: 20,
    backgroundColor: '#4c4d51',
    paddingVertical: 10,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  buttontext: {
    textAlign: 'center',
    color: '#32a852',
    fontFamily: 'sans-serif-thin',
    fontSize: 17,
    fontWeight: 'bold'
  },
  textInput: {
    width: Dimensions.get('screen').width - 30,
    textAlignVertical: 'top',
    paddingLeft: 20,
    paddingRight: 20,
    paddingVertical: 20,
    backgroundColor: 'white',
    marginTop: 15,
    marginVertical: 5,
    borderRadius: 20,
    borderWidth: 1,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    fontFamily: 'sans-serif-thin',
    fontSize: 15,
  },
  errorText: {
    color: 'red'
  },
  iconsheader: {
    width: 50,
    height: 50,
    borderRadius: 100,
    borderWidth: 1,
    backgroundColor: 'gray',
    color: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    marginLeft: 'auto',
    marginTop: 10
  }
});
