import React, { useContext } from 'react';
import { StyleSheet, Text, TouchableOpacity, Dimensions, View } from 'react-native';
import { FirebaseContext } from '@components/Firebase'

export default function Workbook({ navigation }) {
  const firebase = useContext(FirebaseContext);

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('GoalScreen')}>
        <Text style={styles.buttontext}>Goals</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Calendar')}>
        <Text style={styles.buttontext}>Values</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Calendar')}>
        <Text style={styles.buttontext}>Hobbies</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('Calendar')}>
        <Text style={styles.buttontext}>Passions</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#c3ebbb',
  },
  button: {
    marginTop: 30,
    backgroundColor: '#4c4d51',
    paddingVertical: 10,
    borderRadius: 10,
    width: Dimensions.get('screen').width - 100,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection:'row',
  },
  buttontext: {
    textAlign: 'center',
    color: '#08bd0b',
    fontFamily: 'sans-serif-thin',
    fontSize: 17,
    fontWeight: 'bold'
  },
});
