import * as firebase from 'firebase'
import 'firebase/auth'
import 'firebase/database';
import firebaseConfig from './firebaseConfig'

// Initialize Firebase
firebase.initializeApp(firebaseConfig)

const Firebase = {
  // auth
  getAuthUser: () => {
    return firebase.auth().currentUser;
  },
  loginWithEmail: (email, password) => {
    return firebase.auth().signInWithEmailAndPassword(email, password)
  },
  signUpWithEmail: (email, password) => {
    return firebase.auth().createUserWithEmailAndPassword(email, password)
  },
  doPasswordReset: (email) => {
    return firebase.auth().sendPasswordResetEmail(email)
  },
  signOut: () => {
    return firebase.auth().signOut()
  },
  checkUserAuth: user => {
    return firebase.auth().onAuthStateChanged(user)
  },
  // Database
  // User
  createNewUser: uid => {
    return firebase.database().ref(`users/${uid}`)
  },
  // Journal
  createNewJournalEntry: (date, uid) => {
    return firebase.database().ref(`journal/${uid}/${date}`)
  },
  getJournalEntryForDate: (date, uid) => {
    return firebase.database().ref(`journal/${uid}/${date}`)
  },
  // Goals
  createNewGoal: (uid, name) => {
    return firebase.database().ref(`goals/${uid}/${name}`)
  },
  getGoalsForUser: uid => {
    return firebase.database().ref(`goals/${uid}`)
  },
}

export default Firebase
