import React, { useState, useContext } from 'react';
import { StyleSheet, TextInput, Dimensions, View, Text } from 'react-native';

import { getIcon } from './lib';

const TextInputField = ({ value, onChangeValue, password, placeholder, icon }) => {

  return (
    <View style={styles.formfield}>
      {getIcon(icon)}
      <TextInput
        style={styles.textInput}
        onChangeText={text => onChangeValue(text)}
        value={value}
        secureTextEntry={password}
        placeholder={placeholder}
        placeholderTextColor='#4c4d51'
      />
    </View>
  );

}

export default TextInputField;

const styles = StyleSheet.create({
  textInput: {
    width: Dimensions.get('screen').width - 100,
    paddingLeft: 10,
    paddingVertical: 10,
    borderColor: 'black',
    marginVertical: 5
  },
  formfield: {
    width: Dimensions.get('screen').width - 70,
    flexDirection: 'row',
    borderRadius: 15,
    alignItems: 'center',
    backgroundColor: '#32a85230',
    marginVertical: 4,
    paddingLeft: 20
  }
});
