import React from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Octicons } from '@expo/vector-icons';

export const getIcon = (iconName) => {
  if (iconName === 'person') {
    return <Octicons name="person" size={16} color="#4c4d51" />
  } else if (iconName === 'email') {
    return <MaterialCommunityIcons name="email-outline" size={18} color="#4c4d51" />
  } else if (iconName === 'password') {
    return <MaterialCommunityIcons name="key" size={20} color="#4c4d51" />
  }
}
