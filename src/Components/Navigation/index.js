import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import Landing from '@screens/Landing';
import MainNavigator from './MainNavigator';
import AuthNavigator from './AuthNavigator';

const SwitchNavigator = createSwitchNavigator(
  {
    Landing: Landing,
    Auth: AuthNavigator,
    Main: MainNavigator,
  },
  {
    initialRouteName: 'Landing'
  }
)

const AppContainer = createAppContainer(SwitchNavigator)

export default AppContainer
