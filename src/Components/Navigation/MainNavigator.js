import { createStackNavigator } from 'react-navigation-stack';
import JournalEntry from '@screens/JournalEntry';
import Goals from '@screens/Goals';
import HomeTabNavigator from './HomeTabNavigator';

const MainNavigator = createStackNavigator(
  {
    HomeScreen: { screen: HomeTabNavigator, navigationOptions: { headerShown: false }  },
    JournalEntry: { screen: JournalEntry, navigationOptions: { headerTitle: 'Let it flow...' } },
    GoalScreen: { screen: Goals, navigationOptions: { headerTitle: 'What are your goals...' } }
  },
  {
    initialRouteName: 'HomeScreen'
  }
)

export default MainNavigator
