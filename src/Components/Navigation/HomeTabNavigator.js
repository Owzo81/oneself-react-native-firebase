import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import DayScreen from '@screens/DayScreen';
import Calendar from '@screens/Calendar';
import Workbook from '@screens/Workbook';
import SignOut from '@screens/SignOut';
import { FontAwesome } from '@expo/vector-icons';

const MainTabNavigator = createBottomTabNavigator(
  {
    DayScreen: {
      screen: DayScreen,
      navigationOptions: {
        tabBarLabel: 'Home page',
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome name="clock-o" size={24} color={tintColor} />
        )
      },
    },
    Calendar: {
      screen: Calendar,
      navigationOptions: {
        tabBarLabel: 'Calendar',
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome name="calendar" size={24} color={tintColor} />
        )
      },
    },
    WorkBook:  {
      screen: Workbook,
      navigationOptions: {
        headerShown: true,
        tabBarLabel: 'Workbook',
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome name="book" size={24} color={tintColor} />
        )
      },
    },
    SignOut: {
      screen: SignOut,
      navigationOptions: {
        tabBarLabel: 'Sign out',
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome name="sign-out" size={24} color={tintColor} />
        )
      },
    }
  },{
    tabBarOptions: {
      activeTintColor: 'green',
      inactiveTintColor: 'gray',
      showIcon: true
    },
    initialRouteName: 'DayScreen',
  }
)

export default MainTabNavigator
