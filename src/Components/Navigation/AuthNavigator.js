import { createStackNavigator } from 'react-navigation-stack';
import Login from '@screens/Login';
import SignUp from '@screens/SignUp';
import ForgotPassword from '@screens/ForgotPassword';

const AuthNavigator = createStackNavigator(
  {
    Login: { screen: Login, navigationOptions: { headerShown: false } },
    SignUp: { screen: SignUp, navigationOptions: { headerTitle: 'Sign Up' } },
    ForgotPassword: { screen: ForgotPassword, navigationOptions: { headerTitle: 'Forgot Password' } }
  },
  {
    initialRouteName: 'Login'
  }
)

export default AuthNavigator
