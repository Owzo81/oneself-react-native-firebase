import React from 'react';
import { StyleSheet, TouchableOpacity, Dimensions, View, Text } from 'react-native';
import PercentageCircle from 'react-native-percentage-circle';

const GoalTile = ({ item }) => {

  return (
    <TouchableOpacity style={styles.goaltile} onPress={() => {}}>
      <View style={styles.goaltilenamebox}><Text style={styles.goaltiletext}>{item.name}</Text></View>
      <View style={styles.goaltilepercentcompletebox}>
        <PercentageCircle
          color="#3498db"
          radius={45}
          borderWidth={3}
          percent={item.percentComplete}
          color="green"
        >
          <Text style={styles.goaltiletext}>{item.percentComplete}%</Text>
        </PercentageCircle>
      </View>
    </TouchableOpacity>
  )

}

export default GoalTile;

const styles = StyleSheet.create({
  goaltile: {
    justifyContent: 'center',
    flexDirection: 'row',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    width: "100%",
    height: Dimensions.get('screen').height - 750,
    marginTop: "2%",
    backgroundColor: "white",
    borderRadius: 10
  },
  goaltilenamebox: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flex: 1
  },
  goaltilepercentcompletebox: {
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1,
    marginRight: "5%"
  },
  goaltiletext: {
    textAlign: 'center',
    color: 'black',
    fontFamily: 'sans-serif-thin',
    fontSize: 17,
    fontWeight: 'bold'
  },
});
