import React, { useContext, useEffect, useState, useRef } from 'react';
import { StyleSheet, Text, View, Animated, Dimensions, ActivityIndicator } from 'react-native';
import { FirebaseContext } from '@components/Firebase'
import { MaterialIcons } from '@expo/vector-icons';

import { onAddPress, onNextDayPress, onPreviousDayPress } from './lib';

export default function Day(props) {

  const springValue = useRef(new Animated.Value(0.4)).current;
  const [isLoading, setIsLoading] = useState(true);
  const [entry, setEntry] = useState(null);

  const today = new Date();
  const date = props.date ? props.date : today;
  const nextDate = new Date();
  const prevDate = new Date();
  nextDate.setDate(date.getDate() + 1);
  prevDate.setDate(date.getDate() - 1);

  const dateF = date.toLocaleDateString().replace(/\//g,"-");

  useEffect(() => {
    //get enrty from firebase
    props.firebase.getJournalEntryForDate(dateF, props.currentUserId).on('value', snapshot => {
      const entry = snapshot.val();
      if (entry) {
        setEntry(entry);
        setIsLoading(false);
      } else {
        setEntry(null);
        setIsLoading(false);
      }
    });

    const interval = setInterval(() => {
      spring()
    }, 1000);
    return () => clearInterval(interval);

  }, [dateF]);

  const spring = () => {
    Animated.spring(springValue, {
      toValue: 1,
      friction: 1,
      useNativeDriver: true
    }).start()
  }

  const getLocationElement = () => {
    if(entry && entry.longitude && entry.latitude) {
      return <Text style={styles.field}>{entry.longitude} {entry.latitude}</Text>
    } else {
      return <Text style={styles.field}>No location added</Text>;
    }
  }

  let journalelement;
  let locationelement;
  if (entry && !isLoading) {
    journalelement =  <Text style={styles.field}>{entry.journalentry}</Text>
    locationelement = getLocationElement();
  } else if (!isLoading){
    journalelement = <View style={styles.field}>
                <Animated.View style={{ width: 145,
                                      height: 145,
                                      marginTop: "20%",
                                      marginLeft: "35%",
                                      transform: [{scale: springValue}] }}>
                  <MaterialIcons name="add-circle-outline" size={80} color="#4c4d51"
                    onPress={() => onAddPress(props.navigation, dateF, props.currentUserId)} />
                </Animated.View>
              </View>
    locationelement = getLocationElement();
  } else {
    journalelement = <View style={styles.field}>
                <ActivityIndicator size="large"/>
              </View>
    locationelement = <View style={styles.field}>
                <ActivityIndicator size="large"/>
              </View>
  }

  return (
    <View style={styles.container}>
    <View style={styles.dateheader}>
      <MaterialIcons style={styles.buttonback} name="navigate-before" size={50} color="white"
        onPress={() => onPreviousDayPress(props.navigation, prevDate)} />
        <Text  style={styles.dateheadertext}>{dateF}</Text>
      <MaterialIcons style={styles.buttonnext} name="navigate-next" size={50} color="white"
        onPress={() => onNextDayPress(props.navigation, nextDate)} />
    </View>
      <View style={styles.sectioncontainer}>
        <View style={styles.sectionheader}>
          <Text style={styles.sectionheadertext}>Journal Entry</Text>
        </View>
        {journalelement}
      </View>
      <View style={styles.sectioncontainer}>
        <View style={styles.sectionheader}>
          <Text style={styles.sectionheadertext}>Location</Text>
        </View>
        {locationelement}
      </View>
      <View style={styles.sectioncontainer}>
        <View style={styles.sectionheader}>
          <Text style={styles.sectionheadertext}>Pictures/Video</Text>
        </View>
        <Text style={styles.field}>Picture one, picture 2</Text>
      </View>
      <View style={styles.sectioncontainer}>
        <View style={styles.sectionheader}>
          <Text style={styles.sectionheadertext}>Mood</Text>
        </View>
        <Text style={styles.field}>:)</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#c3ebbb',
    flexDirection: 'column'
  },
  sectioncontainer: {
    flex: 2,
    marginTop: 8,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: 20
  },
  dateheader: {
    flex: 1,
    flexDirection: 'row',
    width: Dimensions.get('screen').width - 20,
    borderRadius: 15,
    borderWidth: 1,
    backgroundColor: 'gray',
    color: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10
  },
  dateheadertext: {
    color: 'white',
    fontSize: 18,
    fontFamily: 'sans-serif-thin',
    fontWeight: 'bold'
  },
  sectionheader: {
    width: Dimensions.get('screen').width - 220,
    height: "40%",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    backgroundColor: 'gray',
    color: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
  },
  sectionheadertext: {
    color: 'white',
    fontSize: 18,
    fontFamily: 'sans-serif-thin',
    fontWeight: 'bold'
  },
  field: {
    width: Dimensions.get('screen').width - 20,
    height: "70%",
    flexDirection: 'row',
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderWidth: 1,
    alignItems: 'center',
    paddingLeft: 15,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#fbfcfa',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 10,
    fontFamily: 'sans-serif',
    fontSize: 15,
    color: 'black',
  },
  buttonback: {
    marginRight: 'auto'
  },
  buttonnext: {
    marginLeft: 'auto'
  }
});
