import react from 'react';

export const onAddPress = (navigation, date, currentUserId) => {
  //navigate to the entry screen
  navigation.navigate('JournalEntry',
    {
      date: date,
      currentUserId: currentUserId
    }
  );
}

export const onNextDayPress = (navigation, nextDate) => {
  navigation.navigate('DayScreen',
    {
      date: nextDate,
    }
  );
}

export const onPreviousDayPress = (navigation, prevDate) => {
  navigation.navigate('DayScreen',
    {
      date: prevDate,
    }
  );
}
