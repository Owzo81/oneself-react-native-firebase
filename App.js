import { StatusBar } from 'expo-status-bar';
import React from 'react';
import AppContainer from './src/Components/Navigation'
import { StyleSheet, Text, View } from 'react-native';
import Firebase, { FirebaseContext } from './src/Components/Firebase'
import { YellowBox } from 'react-native';
import _ from 'lodash';

YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

export default function App() {
  return (
    <FirebaseContext.Provider value={Firebase}>
      <AppContainer />
      <StatusBar style="auto" />
    </FirebaseContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
